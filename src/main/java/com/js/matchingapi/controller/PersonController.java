package com.js.matchingapi.controller;

import com.js.matchingapi.model.person_model.PersonChangeRequest;
import com.js.matchingapi.model.person_model.PersonItem;
import com.js.matchingapi.model.person_model.PersonRequest;
import com.js.matchingapi.model.person_model.PersonResponse;
import com.js.matchingapi.service.PersonService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/person")
public class PersonController {
    private final PersonService personService;

    @PostMapping("/join")
    public String setPerson(@RequestBody PersonRequest request){
        personService.setPerson(request);
        return "가입 완료";
    }

    @GetMapping("/all")
    public List<PersonItem> getPersons(){
       return personService.getPersons();
    }

    @GetMapping("/detail/{id}")
    public PersonResponse getPerson(@PathVariable long id){
        return personService.getPerson(id);
    }

    @PutMapping("/change/{id}")
    public String putPerson(@PathVariable long id , @RequestBody PersonChangeRequest request){
        personService.putPerson(id , request);
        return "수정 완료";
    }

    @DeleteMapping("/{id}")
    public String delPerson(@PathVariable long id){
        personService.delPerson(id);
        return "삭제 완료";
    }
}
