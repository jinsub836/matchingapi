package com.js.matchingapi.controller;

import com.js.matchingapi.entity.PersonInfo;
import com.js.matchingapi.model.select_model.SelectItem;
import com.js.matchingapi.model.select_model.SelectRequest;
import com.js.matchingapi.model.select_model.SelectResponse;
import com.js.matchingapi.service.PersonService;
import com.js.matchingapi.service.SelectService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/select")
public class SelectController {
    private final PersonService personService;
    private final SelectService selectService;

    @PostMapping("/new/select-id/{personInfoId}")
    public String setSelectNumber(@PathVariable long personInfoId, @RequestBody SelectRequest request){
        PersonInfo personInfo = personService.getData(personInfoId);
        selectService.setSelectNumber(personInfo ,request);
        return "등록 완료되었습니다.";
    }

    @GetMapping("/all")
    public List<SelectItem> getSelects(){
        return selectService.getSelects();
    }

    @GetMapping("/detail/{id}")
    public SelectResponse getSelect(@PathVariable long id){
        return selectService.getSelect(id);
    }
}
