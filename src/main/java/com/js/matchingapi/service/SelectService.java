package com.js.matchingapi.service;

import com.js.matchingapi.entity.PersonInfo;
import com.js.matchingapi.entity.SelectInfo;
import com.js.matchingapi.model.select_model.SelectItem;
import com.js.matchingapi.model.select_model.SelectRequest;
import com.js.matchingapi.model.select_model.SelectResponse;
import com.js.matchingapi.repository.SelectRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SelectService {
    private final SelectRepository selectRepository;

    public void setSelectNumber(PersonInfo personInfo, SelectRequest request){
        SelectInfo addData = new SelectInfo();
        addData.setSelectDate(LocalDate.now());
        addData.setPersonInfo(personInfo);
        addData.setSelectMobile(request.getSelectMobile());

        selectRepository.save(addData);

    }

    public List<SelectItem> getSelects (){
        List<SelectInfo> originlist = selectRepository.findAll();
        List<SelectItem> result = new LinkedList<>();

        for (SelectInfo select: originlist){
            SelectItem addItem = new SelectItem();
            addItem.setMemberId(select.getPersonInfo().getId());
            addItem.setMemberName(select.getPersonInfo().getName());
            addItem.setMemberMobile(select.getPersonInfo().getMobile());
            addItem.setSelectMobile(select.getSelectMobile());

            result.add(addItem);
        }
        return result;
    }

    public SelectResponse getSelect(long id){
        SelectInfo originData = selectRepository.findById(id).orElseThrow();
        SelectResponse response = new SelectResponse();

        response.setId(originData.getId());
        response.setMemberId(originData.getPersonInfo().getId());
        response.setMakeDate(originData.getPersonInfo().getMakeDate());
        response.setMemberName(originData.getPersonInfo().getName());
        response.setMobile(originData.getPersonInfo().getMobile());
        response.setIsMan(originData.getPersonInfo().getIsMan() ? "남자" : "여자");
        response.setSelectDate(originData.getSelectDate());
        response.setSelectMobile(originData.getSelectMobile());

        return response;
    }

}
