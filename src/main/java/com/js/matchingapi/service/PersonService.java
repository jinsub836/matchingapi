package com.js.matchingapi.service;

import com.js.matchingapi.entity.PersonInfo;
import com.js.matchingapi.model.person_model.PersonChangeRequest;
import com.js.matchingapi.model.person_model.PersonItem;
import com.js.matchingapi.model.person_model.PersonRequest;
import com.js.matchingapi.model.person_model.PersonResponse;
import com.js.matchingapi.repository.PersonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PersonService {
    private final PersonRepository personRepository;

    public PersonInfo getData(long personInfoId){
        return personRepository.findById(personInfoId).orElseThrow();
    }
    public void setPerson(PersonRequest request){
        PersonInfo addData = new PersonInfo();
        addData.setMakeDate(LocalDate.now());
        addData.setName(request.getName());
        addData.setIsMan(request.getIsMan());
        addData.setMobile(request.getMobile());

        personRepository.save(addData);
    }


    public List<PersonItem> getPersons(){
        List<PersonInfo> originlist = personRepository.findAll();
        List<PersonItem> result = new LinkedList<>();


        for(PersonInfo personInfo : originlist){
            PersonItem addItem = new PersonItem();
            addItem.setId(personInfo.getId());
            addItem.setName(personInfo.getName());
            addItem.setIsMan(personInfo.getIsMan());
            addItem.setMobile(personInfo.getMobile());

            result.add(addItem);
        } return result;
    }

    public PersonResponse getPerson(long id){
        PersonInfo originData = personRepository.findById(id).orElseThrow();
        PersonResponse response = new PersonResponse();
        response.setId(originData.getId());
        response.setMakeDate(originData.getMakeDate());
        response.setName(originData.getName());
        if(originData.getIsMan().equals(true)){
            response.setIsMan("남자");} else {
            response.setIsMan("여자");}
        response.setMobile(originData.getMobile());

        return response;
    }

    public void putPerson(long id, PersonChangeRequest request){
      PersonInfo originData = personRepository.findById(id).orElseThrow();

      originData.setName(request.getName());
      originData.setIsMan(request.getIsMan());
      originData.setMobile(request.getMobile());

      personRepository.save(originData);
    }

    public void delPerson(long id){
        personRepository.deleteById(id);
    }
}