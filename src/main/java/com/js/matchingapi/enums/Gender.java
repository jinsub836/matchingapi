package com.js.matchingapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Gender {

     MAN("남자",true)
    ,WOMAN("여자",false);

    private final String name;
    private final Boolean isMan;
}
