package com.js.matchingapi.repository;

import com.js.matchingapi.entity.SelectInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SelectRepository extends JpaRepository<SelectInfo, Long> {
}
