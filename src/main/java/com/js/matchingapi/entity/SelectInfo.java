package com.js.matchingapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class SelectInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "memberId",nullable = false)
    private PersonInfo personInfo;

    @Column(nullable = false)
    private LocalDate selectDate;

    @Column(nullable = false, length = 20)
    private String selectMobile;}
