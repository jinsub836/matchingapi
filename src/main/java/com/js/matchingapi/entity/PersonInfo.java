package com.js.matchingapi.entity;

import com.js.matchingapi.enums.Gender;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class PersonInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDate makeDate;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false)
    private Boolean isMan;

    @Column(nullable = false, length = 20 , unique = true)
    private String mobile;
}
