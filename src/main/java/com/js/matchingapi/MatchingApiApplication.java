package com.js.matchingapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MatchingApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MatchingApiApplication.class, args);
    }

}
