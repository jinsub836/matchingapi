package com.js.matchingapi.model.person_model;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PersonItem {

    private Long id;

    private String name;

    private Boolean isMan;

    private String mobile;
}
