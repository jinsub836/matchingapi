package com.js.matchingapi.model.person_model;

import com.js.matchingapi.enums.Gender;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PersonRequest {

    private String name;

    private Boolean isMan;

    private String mobile;
}
