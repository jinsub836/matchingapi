package com.js.matchingapi.model.person_model;

import com.js.matchingapi.enums.Gender;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PersonResponse {

    private Long id;

    private LocalDate makeDate;

    private String name;

    private String isMan;

    private String mobile;

}

