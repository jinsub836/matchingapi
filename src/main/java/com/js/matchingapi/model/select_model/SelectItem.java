package com.js.matchingapi.model.select_model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SelectItem {

    private Long memberId;

    private String memberName;

    private String memberMobile;

    private String selectMobile;

}
