package com.js.matchingapi.model.select_model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SelectRequest {
    private String selectMobile;
}
