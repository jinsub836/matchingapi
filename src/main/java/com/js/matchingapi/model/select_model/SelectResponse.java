package com.js.matchingapi.model.select_model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class SelectResponse {
    private Long id;

    private Long memberId;

    private LocalDate makeDate;

    private String memberName;

    private String isMan;

    private String mobile;

    private LocalDate selectDate;

    private String selectMobile;

}
